package main

import (
	"context"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/service/codepipeline"
)

type CronEvent struct {
}

func main() {
	lambda.Start(HandleRequest)
}

func HandleRequest(ctx context.Context, evt CronEvent) {
	p := &codepipeline.CodePipeline{}
	input1 := &codepipeline.ListPipelinesInput{}

	for {
		output1, _ := p.ListPipelines(input1)

		for _, ps := range output1.Pipelines {

			if skipPipeline(*ps.Name) {
				continue
			}

			input2 := &codepipeline.StartPipelineExecutionInput{Name: ps.Name}
			p.StartPipelineExecution(input2)
		}

		if output1.NextToken != nil && len(*output1.NextToken) > 0 {
			input1 = &codepipeline.ListPipelinesInput{NextToken: output1.NextToken}
		} else {
			break
		}
	}
}

func skipPipeline(name string) bool {
	switch name {
	case "innius-infrastructure":
		return true
	case "innius-pipeline-provision":
		return true
	}
	return false
}
